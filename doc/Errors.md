# Installation errors

If you run the project locally, depending on your computer configuration, you
may encounter some errors while running `npm test`:

Open the corresponding section if the error you get is similar:

<details>
<summary>15 07 2021 14:16:14.335:ERROR [launcher]: No binary for ChromeHeadless browser on your platform.</summary>

If you have the following issue:

```sh
15 07 2021 14:16:14.334:INFO [launcher]: Starting browser ChromeHeadless
15 07 2021 14:16:14.335:ERROR [launcher]: No binary for ChromeHeadless browser on your platform.
  Please, set "CHROME_BIN" env variable.
npm ERR! Test failed.  See above for more details.
```

Karma is watching for your browser. You should have a Chromium installed
(Chrome, Chromium, Opera, Vivaldi, Edge, ...) and tell where to find it.

You can use a workaround by executing the following command:

```sh
export CHROME_BIN=/usr/bin/chromium-browser
# or (depending on how your chromium is installed)
export CHROME_BIN=/usr/bin/chromium
```
</details>
