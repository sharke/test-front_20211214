const assert = require('assert');
const quixote = require('quixote');

describe('Check if ', function () {

    let frame;

    let page, title, navigation, navAside, navLast, introduction,
        introductionBrand, summary, table;

    const brandPrimary = 'rgb(255, 204, 102)';
    const disabledColor = 'rgb(153, 153, 153)';

    before(function (done) {
        frame = quixote.createFrame({
            src: '/base/src/index.html',
        }, done);
    });

    after(function () {
        frame.remove();
    });

    beforeEach(function (done) {
        frame.reload(function() {
            page = frame.get('#page');
            title = frame.get('#title');
            introduction = frame.get('#introduction');
            introductionBrand = frame.get('#introduction .brand');
            navigation = frame.get('#navigation');
            navAside = frame.get('.nav-container-aside');
            navLast = frame.get('#navigation .nav-container li:last-child a');
            summary = frame.get('summary');
            table = frame.get('table');
            done();
        });
    });

    it('navigation takes the specified width', function () {
        assert.equal(
            navigation.getRawStyle('width'),
            '300px',
            'navigation takes the specified width'
        );
    });

    it('title takes the whole remaining width', function () {
        const pageWidth = parseInt(page.getRawStyle('width'), 10);
        const titleWidth = parseInt(title.getRawStyle('width'), 10);
        assert.equal(
            titleWidth,
            pageWidth - 300,
            'title takes the whole remaining width'
        );
    });

    it('title is in brand primary color', function () {
        assert.equal(
            title.getRawStyle('color'),
            brandPrimary,
            'title is in brand primary color'
        );
    });

    it('title is underlined', function () {
        assert.equal(
            title.getRawStyle('text-decoration-line'),
            'underline',
            'title is underlined'
        );
    });

    it('brand is highlighted', function () {
        assert.equal(
            introductionBrand.getRawStyle('background-color'),
            brandPrimary,
            'brand is highlighted'
        );
    });

    it('introduction is justified', function () {
        assert.equal(
            introduction.getRawStyle('text-align'),
            'justify',
            'introduction is justified'
        );
    });

    it('list does not have bullet', function () {
        assert.equal(
            navAside.getRawStyle('list-style-type'),
            'none',
            'list does not have bullet'
        );
    });

    it('navigation takes the whole height', function () {
        const pageHeight = page.getRawStyle('height');
        assert.equal(
            navigation.getRawStyle('height'),
            pageHeight,
            'navigation takes the whole height'
        );
    });

    it('navigation is flex', function () {
        assert.equal(
            navAside.getRawStyle('display'),
            'flex',
            'navigation is flex'
        );
    });

    it('navigation is vertical', function () {
        assert.equal(
            navigation.getRawStyle('flex-direction'),
            'column',
            'display is vertical'
        );
    });

    it('nav-aside is at the end', function () {
        assert.equal(
            navAside.getRawStyle('justify-content'),
            'flex-end',
            'nav-aside is at the end'
        );
    });

    it('disabled items are displayed differently', function () {
        assert.equal(
            navLast.getRawStyle('color'),
            disabledColor,
            'disabled items are in grey'
        );
    });

    it('disabled items are stroked', function() {
        assert.equal(
            navLast.getRawStyle('text-decoration-line'),
            'line-through',
            'disabled items are stroked'
        );
    });

    it('summary should not change color when details is closed', function () {
        assert.notEqual(
            summary.getRawStyle('background-color'),
            brandPrimary,
            'summary should not change color when details is closed'
        );
    });

    it('table should have border', function () {
        assert.equal(
            table.getRawStyle('border-color'),
            brandPrimary,
            'table should have border'
        );
    });
});
