## Project installation

__Note__: You can still change the way to answer the project by clicking on
"IDE", "Git", or "Download" buttons which are at top right bellow the
"Finish test" button.

Before clicking "Finish test" be sure that you have correctly upload your
answer because you will not be able to come back to it.

### IDE

You can test your code by clicking on "Run quick build" it will takes several
seconds to proceed but you can do it as much as you want.

#### Tips

The exercise is only to write a css file. An HTML page is given. You can write
them on your computer to test it in an graphical environment.

If you do this, please, be sure that you **upload your modification before you
run up of time**.

### Repo cloned or project download

You will have to install the project on your own computer.

Before sending your response be sure that you don't include the `node_modules`
directory otherwise your package will be too big.

#### Errors

If you have errors to run your tests, you can have a look to this page:
[Known errors](./doc/Errors.md)

If you don't succeed to run the project, use the IDE interface to avoid
spending too much time.

# Tasks

The purpose is to display a page following such structure:

![Template.png](./doc/Template.png)

All tasks described as "Bonus" won't be tested automatically, and will be
evaluated only if your works on main tasks are corrects.
So avoid to spend too much time on them if you don't have finished the main
tasks.
This could be a discussion in the oral review.

You will be evaluated on the resolution of these different tasks but also on
the code quality.

## Details

Rules should be applied only in the context they are given.

### Global

* width of the navigation section should be set to 300px
* width of other sections should take all the remaining space

Bonus:

* Propose a nice interface which looks great. The constraint described by
tasks should still be respected.

### Title

* Text should be displayed in brand primary color
* Text should be underlined

### Introduction

* Background of direct child elements which have the class `"brand"` should
be in brand primary color
* The whole paragraph should be justified (All lines should start and end with
the same alignment)

### Navigation

The navigation is a `<nav>` element which contains an `<ul>` with all items
(which are `<a>` inside `<li>`)

* The navigation should take the whole height of the page.
* Use a **Flexbox layout** to display the items.
* They should be displayed vertically without any bullet point.
* Items with class `nav-aside` should be displayed at bottom of the navigation
section.
* If an item has the attribute `disabled`, the text color should be in the
disabled color and should be stroked.

Bonus:

* If there is no items (no `<li>`) the whole navigation block should be
hidden, the other blocks should then takes the whole width of the page.

#### Example

```html
<nav id="navigation" class="nav-container nav-blue nav-open">
    <ul class="nav nav-item nav-container">
        <li class="nav-item"><a href="#home">Home</a></li>
        <li class="nav-item"><a href="#products">Products</a></li>
        <li class="nav-item"><a href="#careers">Careers</a></li>
        <li class="nav-item"><a href="#contact" disabled>Contact</a></li>
    </ul>
    <ul class="nav nav-item nav-container-aside">
        <li class="nav-item"><a href="#about">About</a></li>
    </ul>
</nav>
```
![Menu.png](./doc/Menu.png)

### Content

* The `<summary>` background should be in brand primary color when the
`<details>` is expanded.
* The table border should be in brand primary color.

Bonus:

* The rows background should be in alternate colors (of your choice)

## Definition:

* The brand primary color will be: `#FFCC66`.
* The disabled color will be: `#999999`.


## files

The only file that you should modify is `src/index.css`.

You have an example of html page in `src/index.html`.

There are some tests in `test/style.spec.js`, you are free to modify this
file to add your own tests (be sure that they all pass before submitted your
project).
